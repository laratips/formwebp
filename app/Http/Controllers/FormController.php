<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Form;


class FormController extends Controller
{

    public function index()
    {
        $form = Form::orderBy('id', 'desc')->get();

        return view('formulario', [
            'form' => $form
        ]);
    }

    public function store(Request $request)
    {

        $form = new Form();

        $form->first_name = $request->get('first_name');
        $form->last_name = $request->get('last_name');

        $img = $request->file('image');

        $uniqid = uniqid();
        $rand_start = rand(1, 5);
        $name_image = substr($uniqid, $rand_start, 8);


        if ($img != null) {
            Storage::disk('public')->delete('/Form/' . $form->photo);

            $extension = $img->getClientOriginalExtension();
            $extension = strtolower($extension);
            $archivo = strtolower($name_image . "." . $extension);
            \Storage::disk('local')->put('public/Form/' . $archivo, \File::get($img));


            $webPName = $name_image . ".webp";
            // cwebp -q 100 image1.jpg -o image1.webp
            exec("cwebp -q 100 " . storage_path("/app/public/Form/" . $archivo) . " -o " . storage_path("/app/public/Form/" . $webPName));

            $form->photo = $webPName;

            Storage::disk('public')->delete('/Form/' . $archivo);
        }

        $form->save();

        return Redirect()->route('form-index');
    }


}
