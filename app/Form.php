<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = 'students';

    protected $fillable = [
        'first_name',
        'last_name',
        'photo'
    ];

}
