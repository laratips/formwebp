<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <h1>Formulario de inscripcion</h1>
        <form method="post" action="/storeform" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group row">
                <label for="first_name" class="col-sm-3 col-form-label">Game Title</label>
                <div class="col-sm-9">
                    <input name="first_name" type="text" class="form-control" id="first_name" placeholder="first name">
                </div>
            </div>
            <div class="form-group row">
                <label for="last_name" class="col-sm-3 col-form-label">Game Publisher</label>
                <div class="col-sm-9">
                    <input name="last_name" type="text" class="form-control" id="last_name"
                        placeholder=" last_name">
                </div>
            </div>
            <div class="form-group row">
                <label for="image" class="col-sm-3 col-form-label"> Image</label>
                <div class="col-sm-9">
                    <input name="image" type="file" id="image" class="custom-file-input">
                    <span style="margin-left: 15px; width: 480px;" class="custom-file-control"></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-sm-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">Save Form</button>
                </div>
            </div>
        </form>

        @if(count($form) >= 1)
            @foreach ($form as $fr)
                <h1>{{ $fr->first_name }} {{ $fr->last_name }} -- PHOTO {{ $fr->photo }}</h1>
            @endforeach
        @else
            <p>Sin registros para mostrar</p>
        @endif
    </body>
</html>
