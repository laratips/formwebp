<?php


Route::get('/', function () {
    return view('welcome');
});

Route::get('/formulario', 'FormController@index')->name('form-index');

Route::post('/storeform','FormController@store')->name('store-form');
 